ARG SOURCE_IMAGE
ARG RCC_SOURCE_IMAGE
ARG HDL_SOURCE_IMAGE

FROM localhost/base as packages
COPY . /home/user/opencpi
RUN ln -s exports cdk && ./build/install-packages.sh && pip3 install rich

FROM localhost/packages as prereqs
COPY . /home/user/opencpi
RUN ./build/install-prerequisites.sh && rm -rf prerequisites-build

FROM localhost/packages as build
COPY --from=localhost/prereqs /home/user/opencpi/prerequisites /home/user/opencpi/prerequisites
COPY . /home/user/opencpi
RUN ./build/build-opencpi.sh && ./scripts/test-opencpi.sh --no-hdl && rm -rf prerequisites-build

FROM localhost/build as framework
ARG OCPI_HOST
ARG OCPI_OSP
ARG OCPI_COMP
COPY . /home/user/opencpi
RUN /bin/bash -c "source ./cdk/opencpi-setup.sh -r \
    && .gitlab-ci/scripts/register_projects.sh \"${OCPI_OSP}\" \"${OCPI_COMP}\" \
    && .gitlab-ci/scripts/build.py --platform ${OCPI_HOST} --report-path .gitlab-ci/artifacts/job-report.xml \
    && .gitlab-ci/scripts/clean_projects.sh \
    && rm -rf prerequisites-build"

FROM ${SOURCE_IMAGE} as install
ARG OCPI_TARGET
ARG OCPI_TARGET_TYPE
COPY .gitlab-ci/cache /home/user/opencpi
RUN /bin/bash -c "source ./cdk/opencpi-setup.sh -r \
    && .gitlab-ci/scripts/build.py --${OCPI_TARGET_TYPE} ${OCPI_TARGET} --report-path .gitlab-ci/artifacts/job-report.xml \
    && .gitlab-ci/scripts/clean_projects.sh \
    && rm -rf prerequisites-build"

FROM ${RCC_SOURCE_IMAGE} as rcc_platform
FROM ${HDL_SOURCE_IMAGE} as hdl_rcc_platform
ARG OCPI_RCC_PLATFORM
ARG OCPI_HDL_PLATFORM
COPY --from=rcc_platform /home/user/opencpi /home/user/opencpi
RUN /bin/bash -c "source ./cdk/opencpi-setup.sh -r && ocpiadmin deploy platform ${OCPI_RCC_PLATFORM} ${OCPI_HDL_PLATFORM}"

FROM ${SOURCE_IMAGE} as deploy
COPY CHANGELOG.md CONTRIBUTING.md COPYRIGHT LICENSE.txt README.md /home/user/opencpi/
RUN useradd user -d /home/user -M && chown -R user:user /home/user/opencpi
USER user
WORKDIR /home/user/opencpi
ENTRYPOINT ["/bin/bash", "-c", "source /home/user/opencpi/cdk/opencpi-setup.sh -r && exec $@", "/bin/bash"]
CMD ["/bin/bash"]
