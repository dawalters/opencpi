OCPIDEV-BUILD(1)
==============


NAME
----
ocpidev-build - compiles OpenCPI assets


SYNOPSIS
--------
*`ocpidev`* ['<options>'] *`build`* ['<noun>' ['<name>' ]]


DESCRIPTION
-----------
The *`build`* verb compiles (or performs the equivalent operation for the asset)
the OpenCPI assets specified by the noun used
in the command. If no nouns are provided, *`ocpidev`* builds the asset
represented by the current directory. When the plural version of the nouns are used, no name is
specified and all assets of that type under the current directory are built.

Asset types to be specified in the '<noun>' argument are:

*`application`*(*`s`*)::
    Build all (*`applications`*) or a specific ACI application.

*`hdl`*::
    A prefix to indicate an HDL asset in the *hdl* subdirectory of a project.
    Possible HDL assets are:

        *`assembl`*(*`y`*|*`ies`*);;
	Build all or a specified HDL assembly.
	
	*`device`*;;
	Build an HDL device worker in a
	specified library.
	
	*`platform`*(*`s`*);;
	Build all or a specified HDL platform.
	
	*`primitive`*(*`s`*);;
	Build all HDL primitives or a specified HDL primitive
	of a specified type. For a specified HDL primitive,
	possible HDL primitive types are:
	
	    *`core`*:::
	    Build a primitive that results in a synthesized
	    core/netlist.

	    *`library`*:::
	    Build a primitive that results in a library of modules.
	    
*`librar`*(*`y`*|*`ies`*)::
    Build all or a specified component library.

*`project`*::
    Build all assets in a project.
    
*`test`*(*`s`*)::
    Build all or a specified component unit test but do not run.

*`worker`*(*`s`*)::
    Build all or a specified worker.

Unless suppressed by options, the *`build`* verb builds
both the assets and their OpenCPI asset documents.
See the 'OpenCPI Documentation Writer Guide' for details on OpenCPI asset documents.

OPTIONS
-------
In addition to the options common to all OpenCPI tools (see link:opencpi.1.html[opencpi(1)]),
the options below can be specified for the *`build`* verb. 
These options are primarily used to limit what is built
(focus the build process on specific assets of interest), or specify the
platforms and targets to build the assets for.

In the following descriptions, a plus sign (+) after the option indicates
that it can be specified more than once on the command line.

Options When Building Any Asset
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`--doc-only`*::
   Only build the asset's documentation and not the asset itself.
   See the 'OpenCPI Documentation Writer Guide' for
   details on OpenCPI asset documents.

*`--no-doc`*::
   Only build the asset and not the asset's documentation.
   See the 'OpenCPI Documentation Writer Guide'
   for details on OpenCPI asset documents.

Options When Building Projects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`--hdl-assembly=`*'<hdl-assembly>'+::
    Limit the HDL assemblies being built to the specified assembly. This option
    can also be used in the *`hdl/assemblies/`* directory.

*`--no-assemblies`*::
    Don't build the HDL assemblies in the project. Building HDL assemblies
    (and their containers) can be slow.

Options When Building Projects or Libraries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`--hdl`*::
    Limit the assets being built to HDL primitives, workers, platforms
    or assemblies.

*`--rcc`*::
    Limit the assets being built to RCC workers.

Options When Building Projects, Libraries, Tests, RCC Workers or ACI Applications
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`--hdl-rcc-platform=`*'<platform>'+::
    Build RCC/ACI assets for the RCC platforms associated with the
    specified HDL platform. If this option is not used (and *`--rcc-platform`*
    '<platform>' is also not used), the current development software
    platform is used as the single RCC platform used for building.

*`--optimize`*::
    Build RCC/ACI assets with compiler optimization enabled.  This
    results in a separate set of build outputs that do not conflict
    with non-optimized (debug) build outputs, since they are placed
    in a target directory whose name has a "-o" suffix.

*`--rcc-platform=`*'<platform>'+::
    Build RCC/ACI assets for the specified RCC platform. If this
    option is not used (and *`--hdl-rcc-platform=`*'<platform>' is also not used),
    the current development software platform is used as the single
    RCC platform used for building.

*`--workers-as-needed`*::
    When building an HDL assembly, platform configuration, or component unit test suite, automatically
    build the required HDL workers of the configuration.  Use this option
    when building for a platform that was installed using the *`--minimal`* option to
    *`ocpiadmin install platform`*.

*`--artifacts-only`*::
    When building workers or assemblies, use this option to clean up intermediate build files.
    Only artifacts required for building at the next level or deploying onto hardware will be kept.

Options When Building Projects, Libraries, Tests with HDL Workers or other HDL Assets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`--hdl-platform=`*'<hdl-platform>'+::
    Build HDL assets for the specified HDL platform.

*`--hdl-target=`*'<target>'+::
    Build HDL assets for the specified HDL architecture. If only HDL targets
    are specified (and no HDL platforms), containers will not be
    built for assemblies or unit tests.

Options When Building Workers, Tests and other Assets in Libraries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-l`* '<library>', *`--library=`*'<library>'::
    Build the asset located in the specified component library. This option can
    be used to identify a particular library when operating in a
    project with multiple component libraries. It's not needed when
    operating in a project with only a single top-level    
    *`components`* library. Note that the *`library`* noun differs
    from the *`-l`* option in that the *`library`* noun is used when
    building the library itself, while the *`-l`* option is used when
    building an asset inside a library.

Options When Building Tests
~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`--generate`*::
    Only perform the *`generate`* phase of unit tests, not the *`build`* phase.
    See the section "Unit Testing of Workers" in the *OpenCPI Component Development Guide*
    for details on unit test phases.

*`--nothing-error`*::
    Return non-zero exit status if nothing is done.

Options When Building Assets other than Projects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`--export`*::
    Perform project-level export after the build process completes.

EXAMPLES
--------
. Build the current asset or directory and those underneath it, both
assets and asset documents, 
for the *`zed`* HDL platform and the *`xilinx19_2_aarch32`* RCC platform.
+
---------------------------
ocpidev build --hdl-platform=zed --rcc-platform=xilinx19_2_aarch32
---------------------------
+
. Build only the asset documents the *`assets`* project.
Omit the name *`assets`* if inside the *`assets`* project.
+
---------------------------------------
ocpidev build project assets --doc-only
---------------------------------------
+
. Inside the *`assets`* project, build the *`complex_mixer.hdl`*
worker and its asset document in the *`dsp_comps`* library
and build the worker for the *`zed`* HDL platform.
+
----------------------------------------------
ocpidev -l dsp_comps build worker complex_mixer.hdl
                         --hdl-platform=zed                     
----------------------------------------------
+			 
. Inside the *`assets`* project, build the *`dsp_comps`* library
for the *`zed`* HDL platform and *`xilinx19_2_aarch32 RCC`* platform
and do not build any asset documents.
+
--------------------------------------------------
ocpidev build library dsp_comps --hdl-platform=zed
                                --rcc-platform=xilinx19_2_aarch32
				--no-doc
--------------------------------------------------

BUGS
----
See https://www.opencpi.org/report-defects


RESOURCES
---------
See the main web site: <www.opencpi.org>

See the 'OpenCPI Application Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Application_Development_Guide.pdf

See the 'OpenCPI Component Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Component_Development_Guide.pdf

See the 'OpenCPI HDL Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_HDL_Development_Guide.pdf

See the 'OpenCPI Platform Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Platform_Development_Guide.pdf

See the 'OpenCPI RCC Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_RCC_Development_Guide.pdf

See the 'OpenCPI Documentation Writer Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Documentation_Writer_Guide.pdf

SEE ALSO
--------
link:ocpidev.1.html[ocpidev(1)]
link:ocpidev-clean.1.html[ocpidev-clean(1)]
link:ocpidev-create.1.html[ocpidev-create(1)]
link:ocpidev-delete.1.html[ocpidev-delete(1)]
link:ocpidev-refresh.1.html[ocpidev-refresh(1)]
link:ocpidev-register.1.html[ocpidev-register(1)]
link:ocpidev-run.1.html[ocpidev-run(1)]
link:ocpidev-set.1.html[ocpidev-set(1)]
link:ocpidev-show.1.html[ocpidev-show(1)]
link:ocpidev-unregister.1.html[ocpidev-unregister(1)]
link:ocpidev-unset.1.html[ocpidev-unset(1)]
link:ocpidev-utilization.1.html[ocpidev-utilization(1)]

COPYING
-------
Copyright \(C) 2023 OpenCPI www.opencpi.org. OpenCPI is free software:
you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.
